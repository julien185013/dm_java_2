import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
	     //System.out.println("**************"+(a+b));
      return a+b;
    }


    /**                 fonctionne
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
	     if(liste.size()==0){
         //System.out.println("******* null");
         return null;}
         else{
           int res=liste.get(0);
           for(Integer nb : liste){
             if(nb<res){res=nb;}
		         }
             //System.out.println("******* "+res);
             return res;
           } }


    /**                  fonctionne
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
      boolean res=true;
      if(liste.size()==0){
        //System.out.println("taille=0 ->true   valeur: "+valeur+"\n");
        return true;}
      //System.out.println("valeur -> "+valeur);
      for(T element:liste){
        //System.out.println(element);
        String elt=(String)element;// cast
        String val=(String)valeur;// cast
        if(elt.length()>val.length())
          res=false;}
      //System.out.println(res+"\n");
      return res;
    }


    /**                         fonctionne
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> liste3 = new ArrayList<>();
      List<T> liste4 = new ArrayList<>();
      boolean test=true;
      int cpt=0;
      if(liste1.size()==0 && liste2.size()==0){ return liste3; }
      for(int ii=0; ii<liste2.size(); ++ii){//addition des 2 listes
        liste3.add(liste2.get(ii));      }
      for(int i=0; i<liste1.size(); ++i){
        liste3.add(liste1.get(i));      }
      Collections.sort(liste3);//liste résultante triée par ordre croissant.
      T prec=null;
      for(T element: liste3){
        if(prec==null){
          prec=element;
          cpt+=1;
        }
        else{
          if(prec==element){
            cpt+=1;
            if(cpt==2){
            liste4.add(element);}
          }
          if(prec!=element){
            cpt=1;}
          prec=element;
        }
      }
        return liste4;
    }


    /**                    fonctionne
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      //System.out.println("--texte: "+texte+" --");
      List<String> liste = new ArrayList<>();
      if(texte.length()==0){//System.out.println("+++++ taille du texte=0 ->return liste null \n");
      return liste;}
      String mot="";
      for(int i=0; i<texte.length(); ++i){
        if(texte.charAt(i)!='+' && texte.charAt(i)!=' '){
          mot+=texte.charAt(i);
        }
        else{
          if(mot.length()!=0){
            liste.add(mot);
            mot="";
          }mot="";
        }
        if(i==texte.length()-1){
          if(texte.charAt(texte.length()-1)!=' ' && texte.charAt(texte.length()-1)!='+'){
            liste.add(mot);
          }
        }
      }
      if(liste.size()==0){//System.out.println("+++++ taille de ArrayList=0 ->return null\n");
      return null;}
      //System.out.println("");
        return liste;
    }


    /**             fonctionne des fois ~~~~~~~~~~
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      if(texte.length()==0){return null;}
      ArrayList<String> listeMot=new ArrayList<>();
      String mot="";
      boolean test=false;
      int cpt=0; int cpt2=0;
      for(int i=0; i<texte.length(); ++i){
        if(texte.charAt(i)!=' '){          //System.out.println("+++++++ ajout lettre");
          mot+=texte.charAt(i);
          test=true;
          if(cpt2==0){cpt2=1;}
        }
        else{
          if(test){          //System.out.println("+++++++ ajout mot");
          listeMot.add(mot);cpt2+=1;mot="";
          test=false;
        }
        }
        cpt++;
        if(cpt==texte.length()){//verification du dernier caractère de la chaine
          if(texte.charAt(texte.length()-1)!=' '){
            listeMot.add(mot);//cpt2+=1;
          }
        }
      }
      if(texte.charAt(texte.length()-1)==' '){cpt2--;}//decremente le cpt
      Collections.sort(listeMot);
      String prec=null;int c1=0;int c2=0;
      String mlpl="";
      for(String mot2 :listeMot){
        if(prec==null){
          prec=mot2;
          c1+=1;}
        else{
          if(prec.equals(mot2)){
            c1+=1;
          }
          else{
            if(c1>c2){
            mlpl=prec;
            c2=c1;}
            prec=mot2;
            c1=0;
          }
        }
    }
    //System.out.println("***"+mlpl+"****");
    return mlpl;
    }


    /**                   fonctionne
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int nb1=0;
      int nb2=0;
      if(chaine.length()==0){
        //System.out.println("***** "+chaine+" true\n");
        return true;}
      else if(chaine.charAt(0)==')' || chaine.charAt(chaine.length()-1)=='('){
        //System.out.println("***** "+chaine+" false\n");
        return false;}
      else{
        for(int i=0; i<chaine.length(); ++i){
          if(chaine.charAt(i)=='('){
            nb1++;}
          else if(chaine.charAt(i)==')'){
            nb2++;}
            }
          if(nb1==nb2){
            //System.out.println("*****nb1==nb2 "+chaine+" true\n");
            return true;
          }
        }
        //System.out.println("***** "+chaine+" true\n");
        return false;
      }

    /**                  fonctionne
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      //System.out.println("*****   "+chaine);
      int nb1=0;
      int nb2=0;
      int nb3=0;
      int nb4=0;

      boolean res=false;
      if(chaine.length()==0){
        //System.out.println("*****   chaine vide true\n");
        res= true;
      }
      else if(chaine.charAt(0)==')' || chaine.charAt(0)==']' || chaine.charAt(chaine.length()-1)=='[' || chaine.charAt(chaine.length()-1)=='('){
        res=false;
        //System.out.println("*****   "+chaine+" -> "+res+"\n");
        return false;
      }
      else{
        char prec=' ';
      for(int i=0; i<chaine.length(); ++i){
        if(chaine.charAt(i)=='('){
          nb1++;}
        else if(chaine.charAt(i)==')'){
          if(prec=='['){
            res=false;
            //System.out.println("*****   "+chaine+" -> "+res+"\n");
            return false;}
          nb2++;}
        else if(chaine.charAt(i)=='['){
          nb3++;}
        else if(chaine.charAt(i)==']'){
          if(prec=='('){
            res=false;
            //System.out.println("*****   "+chaine+" -> "+res+"\n");
            return false;}
            nb4++;
          }
        prec=chaine.charAt(i);
      }
      if(nb1==nb2 && nb3==nb4){
        res= true;
      }
    }
      //System.out.println("*****   "+chaine+" -> "+res+"\n");
        return res;
    }


   /** 
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      boolean test=false;
        int debut=0;
        int fin=liste.size()-1; // indice de la dernière valeur
        int centre;
        while(!test && debut<=fin){
          centre=(debut+fin) / 2; //prends l'indice du milieu
          //System.out.println(centre);
          if(liste.get(centre)==valeur){
            //System.out.println(test);
            test=true;
            }
          else if(liste.get(centre)>valeur){
            fin=centre-1;
            }     
          else{
            debut=centre+1;
            }
        }
        return test;
      }

}
